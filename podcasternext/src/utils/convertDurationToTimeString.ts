export function convertDurationToTimeString(duration: number) {
  const hours = Math.floor(duration / 3600);
  const minutes = Math.floor((duration % 3060) / 60);
  const seconds = duration % 60;

  const timeString = [hours, minutes, seconds]
    .map(unit => String(unit).padStart(2, '0 '))
    .join(':');
      
  return timeString;
}

//convertendo segundos para horas   
//Math.floor faz o arredondamento para o menor numero 
//pega o resto da duration