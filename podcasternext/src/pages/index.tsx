// vai determinar o formato da função, a sua tipagem
//a tipagem completa é os parametros e retorno de uma função
import { format, parseISO } from "date-fns"; //o parseISO pega uma string e passa para um formato de date do js
import ptBR from "date-fns/locale/pt-BR";
import { GetStaticProps } from "next";
import Image from 'next/image';
import { api } from "../services/api";
import { convertDurationToTimeString } from "../utils/convertDurationToTimeString";

import styles from './home.module.scss';

type Episodes = {
  id: string,
  title: string,
  thumbnail: string,
  members: string,
  publishedAt: string,
  duration: string,
  durationAsString: string,
  desciption: string,
  url: string,
}
//propriedades que o Home ira receber
type HomeProps = {
  latestEpisodes: Episodes[];
  allEpisodes: Episodes[];
}

export default function Home({ latestEpisodes, allEpisodes }: HomeProps) {
  return (
    <div className={styles.homepage}>
      <section className={styles.latestEpisodes}>
        <h2>Últimos lançamentos</h2>

        {/* no ract sempre que queremos retornar uma lista, é utilizado o map, pos ele percorre algo e retornar */}
        <ul>
          {/* para cada episodio, sera retornado algo */}
          {latestEpisodes.map(episode => {
            //quando é feito um map dentro do react, o primeiro elemento do return, deve ter uma propriedade key, ela é um padrão para o react 
            //e deve ser uma informação unicapara para aquele item, que aqui no caso, é entre cada um dos episodios   
            return (
              <li key={episode.id}>
                {/* é obrigatorio o uso da width e heith, qual é o tamanho que vai ser carregada a imagem */}
                <Image
                  width={192}
                  height={192}
                  src={episode.thumbnail}
                  alt={episode.title}
                  objectFit="cover"
                />

                <div className={styles.episodeDetails}>
                  <a href="">{episode.title}</a>
                  <p>{episode.members}</p>
                  <span>{episode.publishedAt}</span>
                  <span>{episode.durationAsString}</span>
                </div>

                <button type="button">
                  <img src="./play-green.svg" alt="Tocar episódio" />
                </button>
              </li>
            )
          })}
        </ul>
      </section>

      <section className={styles.allEpisodes}>
        <h2>Todos os episódios</h2>

        <table cellSpacing={0}>
          <thead>
            <th></th>
            <th>Podcast</th>
            <th>Integrantes</th>
            <th>Data</th>
            <th>Duration</th>
            <th></th>
          </thead>
          <tbody>
            {allEpisodes.map(episode => {
              return (
                <tr key={episode.id}>
                  <td style={{ width: 72 }}>
                    <Image
                      width={120}
                      height={120}
                      src={episode.thumbnail}
                      alt={episode.title}
                      objectFit="cover"
                    />
                  </td>
                  <td>
                    <a href="">{episode.title}</a>
                  </td>
                  <td>{episode.members}</td>
                  <td style={{ width: 100 }}>{episode.publishedAt}</td>
                  <td>{episode.durationAsString}</td>
                  <td>
                    <button type="button">
                      <img src="/play-green.svg" alt="Tocar episódio" />
                    </button>
                  </td>
                </tr>
              );
            })}
          </tbody>
        </table>
      </section>
    </div>
  )
}

export const getStaticProps: GetStaticProps = async () => { //agora todos os parametros e os retornos estão tipados
  const { data } = await api.get('episodes', {
    params: {
      _limit: 12,
      _sort: 'published_at',
      order: 'desc'
    }
  });//o get vai buscar por alguma informação
  //_limits vai retornar o limete de 12 registros  
  //_sort vai retornar uma ordenação de acordo com a data

  const episodes = data.map(episode => {
    return {
      id: episode.id,
      title: episode.title,
      thumbnail: episode.thumbnail,
      members: episode.members,
      publishedAt: format(parseISO(episode.published_at), 'd MMM y', { locale: ptBR }),
      duration: Number(episode.file.duration),
      durationAsString: convertDurationToTimeString(Number(episode.file.duration)),
      description: episode.description,
      url: episode.file.url
    }
  });

  //vai pergar os dois últinos episodes
  const latestEpisodes = episodes.slice(0, 2); //o slice vai pegar a partir da posição 0 
  const allEpisodes = episodes.slice(2, episodes.length)//para o restante dos episodios, o slice vai pegar a partir da posição 2 e todo o tamanho do array

  //vai retornar quais props estão sendo usadas
  return {
    props: {
      latestEpisodes, //são os dados formatados 
      allEpisodes
    },
    revalidate: 60 * 60 * 8//tempo de quanto tempo a pagina sera atualizada (60 segundos * 60 * 8 = 8 horas)
  }
}