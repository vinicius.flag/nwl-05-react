import {useState} from 'react';

export default function Button (props) { // o props vai receber todas a propiedades que estão sendo utilizadas nos componentes
    //o estado é uma informação que pode ser armazenada no componente, assim sempre que for realizado uma ação, o valor pode ser alterado 
    const [cont, setCont] = useState(1);
    //o useStates retorna duas coisas, (estado, e uma funcção que altera o estado) [state, alteraState]
    function increment() {
        setCont(cont + 1);
    }

    return (
        <>
            {/* // <button>{props.title}</button> // para que ele receba as props, dever ser utilizado como se fosse uma variavel em js. utilizado as chaves */}
            <span>{cont}</span>
            <button onClick={increment}>{props.children}</button> {/*//é possivel utilizar o método de children  */}
            <br />
        </>
    )
}