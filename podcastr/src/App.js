import Button from "./Button";

function App() {
  return (
    //não é possivel adicionar mais de um elemento, eles devem estar sendo englobados por outra tag, seja ela uma DIV, ou uma tag "em branco" <></> (fragment)
    //é possivel colocar propriedades nestes elementos
    <>
      {/* <Buttom title="Buttom 1"/> utilizando com props*/}
      {/*Utilizando o formato de children */}
      <Button>Button 1</Button>  
      <Button>Button 2</Button>
      <Button>Button 3</Button>
      <Button>Button 4</Button>
      <Button>Button 5</Button>
    </>
  );  
}

export default App;
