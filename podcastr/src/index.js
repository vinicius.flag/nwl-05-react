import React from 'react';
import ReactDOM from 'react-dom';//dom é o documento HTML em forma de Objeto no JS, ele é a arvore de elementos 
import App from './App'; //é o outro documento responsavel pelos componentes

ReactDOM.render( //o render vai jogar os elementos para a "tela"
    <App />,//este APP é uma funcção que retorna o HTML, ele é um componente
  document.getElementById('root')
);

